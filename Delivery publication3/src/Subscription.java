
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.ParseException;

public class Subscription {
	private int id;
	private String dateStart;
	private String dateEnd;
	private Database myDB = null;
	private boolean independent;
	private boolean mirror;
	private boolean leader;
	private boolean topic;
	
	public Subscription(String ds, String de, boolean independent, boolean mirror, boolean leader, boolean topic, Database d) {
		this.dateStart = ds;
		this.dateEnd = de;
		this.independent = independent;
		this.mirror = mirror;
		this.leader = leader;
		this.topic = topic;
		myDB = d;
	}

	public boolean insertSubscription() 
	{
		boolean insert = false;
		try 
		{
			insert = myDB.addNewSubscription(dateStart, dateEnd, independent, mirror, leader, topic);
		}
		catch (SQLException | ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return insert;
	}
	public boolean modifySubscription(int id, String dateStart, String dateEnd, boolean independent, boolean mirror, boolean leader, boolean topic) throws SQLException{
		boolean modify = false;
		this.id=id;
		this.dateStart=dateStart;
		this.dateEnd=dateEnd;
		this.independent=independent;
		this.leader=leader;
		this.mirror=mirror;
		this.topic=topic;
		modify=myDB.modifySubscription_SubID(id, dateStart, dateEnd, independent, mirror, leader, topic);
		return modify;
		
	}
	public ResultSet searchSubscription_SubID(int id) throws SQLException {
		ResultSet search=myDB.searchSubscription_SubID(id);
		return search;
		
	}
	public ResultSet searchSubscription_CustID(int id) throws SQLException {
		ResultSet search=myDB.searchSubscription_CusID(id);
		return search;
		
	}

}
